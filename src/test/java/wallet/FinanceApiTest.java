package wallet;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import static org.junit.Assert.*;


public class FinanceApiTest {

    @Test
    public void convert() throws Exception {
        final String convert = new FinanceApi().convert("EUR", "USD", "10");
        System.out.println(convert);
        Assertions.assertThat(convert).isNotEmpty();
    }

}
